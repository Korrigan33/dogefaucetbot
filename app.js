// Create the configuration
var config = require('./config.js');
// Get the lib
var irc = require("irc"),
    database = require('./database.js'),
    accounting = require('accounting'),
    dateFormat = require('dateformat');

console.log('Starting DogeFaucetBot, joining channels', config.channels);
// Create the client name
var client = new irc.Client(config.server, config.botName, {
    channels: config.channels
});

var balance = 0,
    tips = [];

function withdraw_if_needed() {
    if (balance >= 1000) {
        console.log('Balance ', balance, ' Withdrawing');
        client.say('fido', 'withdraw DSfk4pCKcSPLNZNReLX2zGyBdSvu78JAs4');
        balance = 0;
    }
}

// Listen for joins
client.addListener('motd', function () {
    //Identify
    client.say('NickServ', 'identify ' + config.password);

});
client.addListener("join", function (channel, who) {
    if (who === client.nick) {
        console.log("I", client.nick, 'JOINED', channel);
    }
});
client.addListener('notice', function(nick, to, text, message) {
    if(nick === 'NickServ' && to === client.nick && text.match(/^You are now identified for/)) {
        console.log('I\'m identifyed, asking for balance');
        client.say('fido', 'balance');
    }
});
client.addListener('error', function(error) {
    console.log('error', error);
});
client.addListener('message', function (from, to, message) {
    //Private message
    var tip = false;
    if (from === 'fido' && to === client.nick) {
        if ((res = message.match(/ (.*) just sent you Ð([0-9]+)\./))) {
            tip = {
                from: res[1],
                amount: Number(res[2])
            };
            client.say(tip.from, 'Thanks you ! Your tip will be forwarded to the faucet !');
        }else if((res = message.match(/^Your confirmed balance: Ð([0-9]+) /))) {
            balance = Number(res[1]);
            console.log('Received balance:', balance);
            withdraw_if_needed();
        }
    } else if ((from === 'fidoge' || from === 'fido0') && to.substr(0, 1) === '#') {
        if ((res = message.match(/ (.*) sent Ð([0-9]+) to DogeFaucetCom!/i))) {
            tip = {
                from: res[1],
                amount: Number(res[2])
            };
            client.say(to, tip.from.replace(/^\s+|\s+$/g, '') +
                ': Thanks ! Much appreciated, tip will be forwarded to the faucet !' +
                ' \'/msg ' + client.nick + ' help\' for more info.');
        }
    }
    if (tip && !isNaN(tip.amount)) {
        tips.push(tip);
        balance += tip.amount;
        withdraw_if_needed();
        return;
    }
    if (to === client.nick) {
        switch (message) {
            case 'debug':
                if (from == 'Korri') {
                    client.say(from, 'Balance: ' + balance);
                    client.say(from, 'Tips:');
                    client.say(from, JSON.stringify(tips));
                    break;
                }
            case 'help':
            default:
                client.say(from, 'I\'m DogeFaucetCom, a dogefaucet.com IRC bot, every tip I receive is forwarded to the faucet.');
                client.say(from, 'For now all tips, will show on the faucet as donated by "DogeFaucetBot".');
                client.say(from, 'Tips are withdrawn when balance reaches Ð1000.');
                client.say(from, 'Available commands:');
                client.say(from, '    help - show this text');
                client.say(from, '    history - show faucet donations history');
                client.say(from, '    top - show faucet top contributors');
                break;
            case 'history':
                database.Transaction.collection()
                    .query(function (qb) {
                        qb.orderBy('id', 'DESC')
                            .limit(15)
                            .where('donation', 1);
                    })
                    .fetch().then(function (collection) {
                        client.say(from, 'Donations history:');
                        collection.each(function (transaction) {
                            var date = new Date(transaction.get('time') * 1000);
                            client.say(from,
                                '   ' +
                                    (transaction.get('username') || "Anonymous") +
                                    ' donated ' +
                                    accounting.formatMoney(transaction.get('amount'), 'Ð', 8, ' ', '.') +
                                    ' the ' +
                                    dateFormat(date, 'mmmm dS, yyyy')
                            );
                        });
                    });
                break;
            case 'top':
                database.Donator.collection()
                    .query(function (qb) {
                        qb.where('amount', '>', 0)
                            .orderBy('amount', 'DESC')
                            .limit(20);
                    })
                    .fetch()
                    .then(function (collection) {
                        client.say(from, 'TOP20 contributors:');
                        collection.each(function (donator) {
                            client.say(from,
                                '   ' +
                                    donator.get('username') +
                                    ' ' +
                                    accounting.formatMoney(donator.get('amount'), 'Ð', 0, ',', '.')
                            )
                        });
                    });
                break;
        }
    }
});
