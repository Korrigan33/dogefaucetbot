(function(database){
    "use strict";
    var Bookshelf = require('bookshelf');

    var MySql = Bookshelf.initialize({
        client: 'mysql',
        connection: {
            host     : '127.0.0.1',
            user     : 'faucet',
            password : 'AuWM2dXQNN4FENjZ',
            database : 'faucet',
            charset  : 'utf8_unicode_ci'
        }
    });

    database.Transaction = MySql.Model.extend({
        tableName: 'transactions'
    });
    database.Donator = MySql.Model.extend({
        tableName: 'donators'
    })
}(exports))
